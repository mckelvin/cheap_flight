# coding: utf-8
import os
import pdb
import shelve
import hashlib
import requests
from datetime import datetime, date
from time import time, sleep
import json


DATA_DIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', 'data'))


class BaseSearcher(object):
    pass


class TaobaoTripSearcher(BaseSearcher):

    INDEX_URL = 'http://h5.m.taobao.com/trip/flight/search/index.html'
    shelf_path = os.path.join(DATA_DIR, 'taobao_trip.shelf')

    def __init__(self):
        self.http_session = requests.Session()
        self.http_session.get('http://api.m.taobao.com/rest/h5ApiUpdate.do')
        self.db = shelve.open(self.shelf_path, 'c')

    def __del__(self):
        self.http_session.close()
        if self.db is not None:
            self.db.close()
            self.db = None

    def _flight_search(self, dep_airport, arr_airport, dep_date, arr_date):
        # http://h5.taobao.org/help/apidoc/files/lib-mtop_src_core_base.js.html
        app_key = '12574478'
        cookie_uk = self.http_session.cookies.get('_m_h5_tk', '').split('_')[0]
        time_seconds = int(time())
        query_str = (
            '{"depCityCode":"%s","arrCityCode":"%s",'
            '"depDate":"%s","itineraryFilter":"0",'
            '"cabinClassFilter":"0","sellerIds":""}' % (
                dep_airport, arr_airport, dep_date
            ))
        uni_str = '&'.join([cookie_uk, str(time_seconds), app_key, query_str])
        sign = hashlib.md5(uni_str).hexdigest()
        payload = dict(
            callback='_',
            type='jsonp',
            api='mtop.trip.flight.flightSearch',
            v='1.0',
            data=query_str,
            ttid='201300@travel_h5_3.1.0',
            appKey=app_key,
            sign=sign,
            t=time_seconds
        )
        res = self.http_session.get(
            'http://api.m.taobao.com/rest/h5ApiUpdate.do',
            params=payload)

        text = res.text.strip().lstrip('_ (').rstrip(') ')
        data = json.loads(text, 'utf-8')
        return data

    def flight_search(self, dep_airport='BJS', arr_airport='XNN',
                      dep_date=date(2014, 04, 25), arr_date=None):
        def is_successful(data):
            return (data and
                    data.get('ret', [''])[0].split('::')[0] == 'SUCCESS')

        cache_key = 'flight_search:%s:%s:%s:%s' % (
            dep_airport, arr_airport, dep_date, arr_date)
        data, last_modify = self.db.get(cache_key, (None, None))
        if not is_successful(data) or time() - last_modify > 60:
            for i in range(2):
                # first request without cookie is destined to fail
                data = self._flight_search(dep_airport, arr_airport,
                                           dep_date, arr_date)
                if is_successful(data):
                    self.db[cache_key] = (data, time())
                    break
            else:
                raise
        return data.get('data')

    def _load_common_cheapest_flight(self, rule_id, force=False):
        cache_key = 'common_cheapest_flight:rule%s' % rule_id
        data, last_modify = None, None
        if not force:
            data, last_modify = self.db.get(cache_key, (None, None))
        if not data or time() - last_modify > 3600 or force:
            payload = dict(
                ruleId=rule_id,
                callback='_'
            )
            res = self.http_session.get(
                'http://s.jipiao.trip.taobao.com/search'
                '/commonCheapestFlight.htm',
                params=payload
            )
            text = res.text.strip().lstrip('_ (').rstrip(') ')
            data = json.loads(text, 'utf-8')
            self.db[cache_key] = (data, time())
            sleep(1)
        return data

    def _process_rule_data(self, data):
        rule_name = data.get('name')
        city_code_list = data.get(
            'cityCodeList').encode('utf-8').split(',')
        city_name_list = data.get(
            'cityNameList').encode('utf-8').split(',')
        for a, b in zip(city_code_list, city_name_list):
            key = 'airport:IATA:%s' % a
            if key not in self.db:
                self.db[key] = b
        for flight_dct in data.get('trip'):
            dep_code = flight_dct.get('depCode').encode('utf-8')
            dep_name = flight_dct.get('depName').encode('utf-8')
            arr_code = flight_dct.get('arrCode').encode('utf-8')
            arr_name = flight_dct.get('arrName').encode('utf-8')

            price_info_list = flight_dct.get('priceInfo')
            for price_info_dct in price_info_list:
                price = price_info_dct.get('price')
                discount = float(price_info_dct.get('discount'))
                date_ = datetime.strptime(
                    price_info_dct.get('date'), '%Y-%m-%d')
                if (dep_name in {'北京', '天津'} and
                        # arr_name in {'杭州'} and
                        discount < 3):
                    print '%s %s(%s) - %s(%s) ￥%d / %s' % (
                        date_, dep_code, dep_name,  arr_code,
                        arr_name, price, discount
                    )

    def load_common_cheap_flight(self):
        min_rule_id = self.db.get('min_rule_id', 1)
        max_rule_id = self.db.get('max_rule_id', 500)
        err_count = 0
        max_err_count = 3
        valid_rule_ids = []
        for rule_id in range(min_rule_id, max_rule_id):
            dct = self._load_common_cheapest_flight(rule_id)
            if dct.get('error') != 0:
                err_count += 1
                if err_count >= max_err_count:
                    self.db['max_rule_id'] = rule_id - err_count
                    break
                continue
            else:
                err_count = 0
                self._process_rule_data(dct.get('data'))
            valid_rule_ids.append(rule_id)
        self.db['valid_rule_ids'] = valid_rule_ids


if __name__ == '__main__':
    tt = TaobaoTripSearcher()
    # tt.load_common_cheap_flight()
    res = tt.search()
