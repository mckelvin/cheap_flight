# coding: utf-8
import sys
from datetime import date, datetime, time
from model.taobao_trip import TaobaoTripSearcher
try:
    import pync
except ImportError:
    pync = None


tt_searcher = TaobaoTripSearcher()


def notify(dct):
    if sys.platform == 'darwin':
        if pync is not None:
            print 'biu'
            pync.Notifier.notify(
                str('{flightName}: ￥{bestPrice}'.format(**dct)),
                execute='say "cheap flight found"')


def fit_rule1(flt_dct):
    al_name = flt_dct.get('flightName')
    al_share = flt_dct.get('share')
    best_price = int(flt_dct.get('bestPrice'))
    dep_dt = datetime.strptime(flt_dct.get('depTime'), '%Y-%m-%d %H:%M')
    dep_time = dep_dt.time()
    if time(18, 00) <= dep_time < time(21, 30):
        print dep_time, (al_share if al_share else al_name), best_price
        if best_price < 400:
            return True


def monitor_flight(rule_fns=[fit_rule1]):
    data = tt_searcher.flight_search('BJS', 'XNN', date(2014, 4, 25))
    if int(data['remindSubscribe']['remindSubscribeTotal']) != 0:
        print data['remindSubscribe']
        raise NotImplemented

    for flt_dct in data.get('flights', []):
        for rule_fn in rule_fns:
            if rule_fn(flt_dct):
                notify(flt_dct)
