# coding: utf-8

import time
from view.taobao_trip import monitor_flight


if __name__ == '__main__':
    while True:
        try:
            print time.asctime()
            monitor_flight()
            time.sleep(5 * 60)
        except KeyboardInterrupt:
            break
